﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour
{

	public PlayerMovement charMov;
	public PlayerStatus playerStatus;
	Target playerTarget;
	int range = 100;
	bool recivedTarget = false;
	public GameObject pointerPrefab;
	GameObject pointer;
	int selectableMask;
	/*float selectionTime = 0.5;
	bool verifySelection = false;

	float timer;*/
	void Start(){
//		setTriggerAction (new PlayerDestination(transform.position,PlayerDestination.DestinationType.None));
		selectableMask = LayerMask.GetMask ("Selectable");
	}

	void Update ()
	{
		/*if (verifySelection){
			timer+=Time.deltaTime;
			if (timer>selectionTime){
				timer = 0;
				verifySelection = false;
				if (!recivedTarget && playerTarget!=null){

					playerTarget = null;
				}
				recivedTarget=false;
			}
		}*/
		if (Input.GetMouseButtonDown (0) && charMov.moveState != PlayerMovement.MoveState.Jump) {
			if (!recivedTarget && playerTarget!=null) {
				playerTarget.NotTargeting ();
			}
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit rayHit;
			if (Physics.Raycast (ray, out rayHit, range)) {
				if (pointer!=null){
					Destroy(pointer);
				}
				//Debug.Log("rayHit: "+rayHit.collider.name + " " + rayHit.transform.position);
				pointer = Instantiate (pointerPrefab, rayHit.point, Quaternion.identity) as GameObject;
				PlayerDestination destination;
				if (rayHit.collider.CompareTag ("Portal")) {
					//charNav.navAgent.destination = new Vector3(rayHit.transform.position.x,rayHit.transform.position.y-4,rayHit.transform.position.z);
					destination = new PlayerDestination(new Vector3 (rayHit.transform.position.x, rayHit.transform.position.y - 4, rayHit.transform.position.z),PlayerDestination.DestinationType.Portal);
					charMov.SetDestination (destination);
				} else {
					//charNav.navAgent.destination = rayHit.point;
					PlayerDestination.DestinationType target;
					if (rayHit.collider.CompareTag ("Jump")){
						target = PlayerDestination.DestinationType.Jump;
					}else{
						target = PlayerDestination.DestinationType.None;
					}
					destination = new PlayerDestination(rayHit.point,target);
					charMov.SetDestination (destination);
				}
				if (charMov.navAgent.hasPath) {
					playerStatus.ModifyDestiny (PlayerDestination.DestinationType.None);
				}
				setTriggerAction (destination);
				if (!charMov.onJump)
					charMov.readyToJump = true;
			}
			recivedTarget = false;
		}
	}

	void OnEnable ()
	{
		Target.ImTheTarget += TargetObject;
	}
	
	void OnDisable ()
	{
		Target.ImTheTarget -= TargetObject;
	}

	public void TargetObject (Target target)
	{
		playerTarget = target;
		recivedTarget = true;
	}

	void setTriggerAction (PlayerDestination destination)
	{
		//Debug.Log ("walkReg: "+walkReg+" statusReg: "+(playerStatus.statusActionReg & 3));
		if (playerStatus.isOnAction ()) {
			//int flag = ((playerStatus.statusActionReg & 3) ^ walkReg) ;
			//Debug.Log("Flag: "+flag);
			byte walkReg = 1;
			//if (((playerStatus.statusActionReg & 3) ^ walkReg)!= 0) {
			playerStatus.statusActionReg &= 252;
			playerStatus.statusActionReg |= walkReg;
			//Debug.Log ("Capture ");
			playerStatus.TraceAction (destination);
			//}
		}
	}
}
