﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverManager : MonoBehaviour {

	public PlayerHealth playerHealth;
	public float restartDelay = 5f;
	public Text textGameOver;
	
	
	Animator anim;
	float restartTimer;

	void Awake ()
	{
		anim = GetComponent <Animator> ();
	}
	
	
	void Update ()
	{
		if(UserWin.levelwin)
		{
			FinishLevel("Level Finished!", Application.loadedLevel+1);
		}else if (TimerManager.gameover){
			FinishLevel("Game Over!",Application.loadedLevel);
		}
	}

	void FinishLevel(string finalText, int level){
		textGameOver.text = finalText;
		anim.SetTrigger ("GameOver");
		restartTimer += Time.deltaTime;
		if(restartTimer >= restartDelay)
		{
			//Application.LoadLevel(level);
		}
	}
}