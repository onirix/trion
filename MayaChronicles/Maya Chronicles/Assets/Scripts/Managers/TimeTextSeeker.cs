﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeTextSeeker : MonoBehaviour {

	public static int time;
	
	
	Text text;
	
	
	void Awake ()
	{
		text = GetComponent <Text> ();
		time = 0;
	}
	
	
	void Update ()
	{
		text.text = "Time: " + time;
	}
}
