using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TimerManager : MonoBehaviour
{
	public delegate void RotationReset ();
	public static event RotationReset OnRotationReset;

	public float catchTime = 3f;
	public GameObject playerEcho;
	public GameObject player;
	float timer;
	float timeDelayReturn = 2f;
	int timeLineLimit = 7;
	int currentTimeLine = -1;
	List<int> currentMoments;
	List<TimeLineEvents> timeLineEvents;
	List<TimeEvent> eventsPending;
	List<GameObject> playerEchos;
	public static bool isTimeStop = false;
	public static bool gameover = false;

	void Start ()
	{

		currentMoments = new List<int> ();
		timeLineEvents = new List<TimeLineEvents> ();
		eventsPending = new List<TimeEvent> ();
		playerEchos = new List<GameObject> ();

		newTimeLine ();
	}

	void Update ()
	{
		timer += Time.deltaTime;
		TimeTextSeeker.time = (int) timer;
		if (isTimeStop && timer > timeDelayReturn) {
			isTimeStop = false;
			timer = 0;
		}
	}

	void OnEnable ()
	{
		PlayerStatus.OnAction += CatchEvent;
		PlayerStatus.OnTargetChanged += TargetChanged;
		PortalAction.OnGoToPass += BackToPass;
		PlayerEchoEvent.EventReady += DispatchEvent;
		FallDeath.OnFallDeath += PlayerDeath;
		PlayerEchoHealth.OnFindNewTimeLine += FindNewTimeLine;
	}

	void OnDisable ()
	{
		PlayerStatus.OnAction -= CatchEvent;
		PlayerStatus.OnTargetChanged -= TargetChanged;
		PortalAction.OnGoToPass -= BackToPass;
		PlayerEchoEvent.EventReady -= DispatchEvent;
		FallDeath.OnFallDeath -= PlayerDeath;
		PlayerEchoHealth.OnFindNewTimeLine -= FindNewTimeLine;
	}

	void CatchEvent (byte actionsReg, PlayerDestination playerTarget)
	{
		timeLineEvents [currentTimeLine].events.Add (new TimeEvent (timer, actionsReg, playerTarget));
		/*if ((actionsReg&4)==4){
			BackToPass();
		}*/
	}

	void TargetChanged (PlayerDestination playerTarget)
	{
		if (timeLineEvents [currentTimeLine].events.Count > 0){
			timeLineEvents [currentTimeLine].events [timeLineEvents [currentTimeLine].events.Count - 1].playerTarget = playerTarget;
		}
	}

	void PlayerDeath(){
		int i = playerEchos.Count-1;
		do{
			if (playerEchos[i].gameObject==null){
				DeleteEchoTrace(i);
				i--;
			}else{
				InstantiateNewPlayer(i);
				return;
			}
		}while(i>=0);
		gameover = true;
	}

	void FindNewTimeLine(int indexEcho)
	{
		GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>().TakeDamage(100);
		int i = playerEchos.Count-1;
		do{
			if (playerEchos[i].gameObject!=null){
				Destroy(playerEchos[i].gameObject);
			}
			DeleteEchoTrace(i);
			i--;
		}while(i>=indexEcho);

		if (playerEchos.Count>0){
			InstantiateNewPlayer(playerEchos.Count-1);
		}
	}

	void DeleteEchoTrace(int i)
	{
		playerEchos.RemoveAt(i);
		eventsPending.RemoveAt(i);
		currentMoments.RemoveAt(i);
		timeLineEvents.RemoveAt(i);
		currentTimeLine--;
	}

	void InstantiateNewPlayer(int i){
		Vector3 echoPosition = playerEchos[i].gameObject.transform.position;
		Quaternion echoRotation = playerEchos[i].gameObject.transform.rotation;
		Destroy(playerEchos[i].gameObject);
		playerEchos.RemoveAt(i);
		Instantiate(player,echoPosition,echoRotation);
	}

	void DispatchEvent (int i)
	{
		if (!isTimeStop) {
			if ((timeLineEvents [i].events [currentMoments [i]].actionsReg & 4) == 4) {
				Destroy (playerEchos [i].gameObject);
			} else {
				eventsPending [i] = timeLineEvents [i].events [currentMoments [i]];
				playerEchos [i].GetComponent <PlayerEchoEvent> ().setTargetEvent (timeLineEvents [i].events [currentMoments [i]], i);
				currentMoments [i]++;
			}
		}
	}

	void newTimeLine ()
	{
		timeLineEvents.Add (new TimeLineEvents ());
		currentMoments.Add (0);
		eventsPending.Add (new TimeEvent ());
		currentTimeLine++;
	}

	void BackToPass ()
	{
		timer = 0;
		if (currentTimeLine < timeLineLimit) {
			isTimeStop = true;
			// Hay que enviar un mensaje para parar el tiempo
			DestroyEnemys ();
			//DestroyEchos ();
			playerEchos.Add ((GameObject)Instantiate (playerEcho, timeLineEvents [currentTimeLine].events [0].playerTarget.destination, Quaternion.identity /*timeLineEvents [currentTimeLine].events [0].rotation*/));
			isTimeStop = false;
			ResetScene ();
			newTimeLine ();
		}
	}

	void ResetScene ()
	{
		/*
		 * 1.- Llevar a todos lo enrmigos a su estado inicial y destruir los que haya que destruir
		 * 2.- REiniciar las trampas y los switches del laberinto
		 * 3.- Reiniciar asu posicion y estado inical de todos los echos del player que existan
		 */
		OnRotationReset();
		for (int i=0; i<playerEchos.Count; i++) {
			currentMoments [i] = 0;
			if (playerEchos [i] == null){
				playerEchos [i] = (GameObject)Instantiate (playerEcho, timeLineEvents [i].events [0].playerTarget.destination, Quaternion.identity /*timeLineEvents [i].events [0].rotation*/);
			}
			playerEchos[i].name = "PlayerEcho"+i;
			NavMeshAgent playerEchoNav = playerEchos [i].GetComponent <NavMeshAgent>();
			playerEchoNav.Warp(timeLineEvents [i].events [0].playerTarget.destination);
			playerEchos [i].transform.rotation = timeLineEvents [i].events [0].rotation;
			/*playerEchoNav.enabled = false;
			playerEchos [i].transform.position = timeLineEvents [i].events [0].playerTarget.destination;
			playerEchos [i].transform.rotation = timeLineEvents [i].events [0].rotation;
			playerEchoNav.enabled = true;*/
			//playerEchos [i].GetComponent <PlayerEchoEvent>().setTargetEvent(timeLineEvents[i].events[0],i);
			DispatchEvent (i);
		}
	}

	void DestroyEnemys ()
	{
		GameObject[] enemys = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach (GameObject e in enemys) {
			Destroy (e);
		}
	}

	void DestroyEchos ()
	{
		GameObject[] echos = GameObject.FindGameObjectsWithTag ("Echo");
		foreach (GameObject e in echos) {
			//e.GetComponent<NavMeshAgent>().enabled = false;
			Destroy (e);
		}
	}

	private class TimeLineEvents
	{
		public List<TimeEvent> events;

		public TimeLineEvents ()
		{
			events = new List<TimeEvent> ();
		}
	}

}
