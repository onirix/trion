﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour
{
	public delegate void DisableNav (bool disable);
	public static event DisableNav OnDisableNav;

	public delegate void EnableJumps ();
	public static event EnableJumps OnEnableJumps;

	//public delegate void PlayerOnTheFloor ();
	//public static event PlayerOnTheFloor OnPlayerOnTheFloor;

	public Vector3 axis;
	public int isRotate = 0;
	float timeBetwenRotations = 3.5f;
	float timer;
	float deltaAngle = -1f;
	Vector3 positionStart;
	Quaternion rotationStart;
	//Rigidbody rigidbody;
	// Use this for initialization
	//GameObject collider;
	public bool playerOn = false;


	void Awake ()
	{
		axis = Vector3.up;
		//rigidbody = GetComponent<Rigidbody> ();
		//Debug.Log ("Angular i: " + rigidbody.angularVelocity);
		positionStart = transform.position;
		rotationStart = transform.rotation;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		//Debug.Log ("PlayerOn: "+playerOn);
		if (isRotate > 0) {
		//if (isRotate == 1) {
			//rigidbody.AddTorque (transform.up * deltaAngle);
			//Debug.Log ("Angular 0: " + rigidbody.angularVelocity.magnitude+", "+transform.eulerAngles + ", "+((int)transform.eulerAngles.magnitude % 90));
			transform.Rotate (axis * deltaAngle);
			if (transform.eulerAngles.magnitude > 0.1f && (int)transform.eulerAngles.magnitude % 90 == 0) {
				//if (rigidbody.angularVelocity.magnitude > 0.1f && ((int)transform.eulerAngles.magnitude % 90 == 0)) {
				//Debug.Log ("Angular: " + rigidbody.angularVelocity);
				//rigidbody.angularVelocity = Vector3.zero;
				//Debug.Log ("Termine de rotar: " + transform.eulerAngles.magnitude);
				if (isRotate == 1) {
					isRotate = -1;
				} else {
					isRotate = 0;
					Debug.Log ("playerOn: " + playerOn);
					/*if (playerOn)
						OnDisableNav (true);*/
				}
			}

		} else if (isRotate < 0) {
			if (!playerOn) {
				timer += Time.deltaTime;
				if (timer > timeBetwenRotations) {
					timer = 0;
					isRotate = 2;
					deltaAngle = 1f;
					//rigidbody.angularVelocity = ;
				}
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		//other.transform.SetParent (transform);
		if (other.CompareTag ("Player")||other.CompareTag("Echo")) {
			//Debug.Log("Player ON");
			playerOn = true;
			//OnPlayerOnTheFloor ();
			//OnEnableJumps();
		}
	}

	void OnTriggerExit (Collider other)
	{
		//other.transform.parent = null;
		if (other.CompareTag("Player")||other.CompareTag("Echo")){
			//Debug.Log("Player OUT");
			playerOn = false;
		}
	}

	void OnEnable ()
	{
		TimerManager.OnRotationReset += ResetRotation;
	}
	
	void OnDisable ()
	{
		TimerManager.OnRotationReset -= ResetRotation;
	}

	public void setRotation ()
	{
		if (isRotate == 0) {
			isRotate = 1;
			deltaAngle = -1f;
			/*if (playerOn)
				OnDisableNav (false);*/
		}
	}

	void ResetRotation ()
	{
		isRotate = 0;
		timer = 0;
		transform.rotation = rotationStart;
		transform.position = positionStart;
	}
}
