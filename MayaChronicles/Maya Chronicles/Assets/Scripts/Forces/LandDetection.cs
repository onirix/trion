﻿using UnityEngine;
using System.Collections;

public class LandDetection : MonoBehaviour {

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Player")) {
			Debug.Log("Player landing");
			SendMessageUpwards("PlayerOn",true);
		}
	}
	
	void OnTriggerExit (Collider other)
	{
		if (other.CompareTag("Player")){
			Debug.Log("Player out");
			SendMessageUpwards("PlayerOn",false);
		}
	}
}
