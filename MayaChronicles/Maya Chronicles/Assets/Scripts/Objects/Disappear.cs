﻿using UnityEngine;
using System.Collections;

public class Disappear : MonoBehaviour {
	float timer;
	float timeDisappear = 3f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer > timeDisappear){
			Destroy(gameObject);
		}
	}
}
