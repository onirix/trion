﻿using UnityEngine;
using System.Collections;

public class Switch1 : MonoBehaviour {
	public RotateObject objRot;
	Vector3 yposition;
	Vector3 iposition;
	float smooth = 2f;
	bool characterOn = false;

	void Awake()
	{
		iposition = transform.position;
		yposition = new Vector3(transform.position.x,-8f,transform.position.z);
	}
	void FixedUpdate(){
		if (characterOn){
			if (Vector3.Distance(transform.position,yposition)>0.1f){
				transform.position = Vector3.Lerp(transform.position, yposition, smooth * Time.deltaTime);
			}else{
				characterOn = false;
			}
		}else{
			if(Vector3.Distance(transform.position,iposition)>0.1f){
				transform.position = Vector3.Lerp(transform.position,iposition,smooth * Time.deltaTime);
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag("Player")||other.CompareTag("Echo"))
		{
			characterOn = true;
			objRot.setRotation();
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (other.CompareTag("Player")||other.CompareTag("Echo"))
		{
			characterOn = true;
		}
	}
}
