﻿using UnityEngine;
using System.Collections;

public class BlockGrab : Target {
	GameObject player; 

	void Awake () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		/*if (playerInRange && !PlayerManager.isTarget(gameObject)){
			playerInRange = false;
			player.setGrabStatus(PlayerMovement.MoveState.Idle);
			Debug.Log("Update: grab exit block "+name+" "+Time.realtimeSinceStartup);
		}*/
	}

	void OnMouseDown(){
		if (ifTargetEnabled()){
			isTarget = true;
			setTarget();
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if(isTarget && other.gameObject == player)
		{
			player.GetComponent<PlayerMovement>().setStatus(PlayerMovement.MoveState.Pull);
		}
	} 

	/*void OnTriggerExit(){
		if (playerInRange){
			playerInRange = false;
			player.setGrabStatus(PlayerMovement.MoveState.Idle);
			Debug.Log("OnTriggerExit: grab exit block "+name);
		}
	}*/

}
