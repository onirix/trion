﻿using UnityEngine;
using System.Collections;

public class DisableJump : MonoBehaviour {

	private BoxCollider collider;

	void Awake(){
		collider = GetComponent <BoxCollider>(); 
	}



	void OnEnable ()
	{
		PlayerMovement.OnDisableJumps += Disable;
		RotateObject.OnEnableJumps += Enable;
	}
	
	void OnDisable ()
	{
		PlayerMovement.OnDisableJumps -= Disable;
		RotateObject.OnEnableJumps -= Enable;
	}
	void Disable(){
		collider.enabled = false;
	}
	void Enable(){
		collider.enabled = true;
	}
}
