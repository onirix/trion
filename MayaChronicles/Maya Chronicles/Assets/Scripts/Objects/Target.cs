﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {
	public delegate void TheTarget (Target target);
	public static event TheTarget ImTheTarget;
	public bool isTarget = false;

	public void NotTargeting(){
		isTarget = false;
	}

	public bool ifTargetEnabled(){
		if (ImTheTarget!=null){
			return true;
		}
		return false;
	}

	public void setTarget(){
		ImTheTarget((Target) this);
	}
}
