using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PortalAction : MonoBehaviour
{
	public delegate void GoToPass ();

	public static event GoToPass OnGoToPass;

	GameObject player;
	public Transform portalExit;
	public Image timePassImage;
	public Color flashColour = new Color(0f, 1f, 1f, 0.1f); 
	public float flashSpeed = 5f; 
	bool portalOpen = true;
	bool passaje = false;

	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		timePassImage = GameObject.Find("TimePassImage").GetComponent<Image>();
	}

	void Update ()
	{
		if(passaje)
		{
			timePassImage.color = flashColour;
		}
		else
		{
			timePassImage.color = Color.Lerp (timePassImage.color, Color.clear, flashSpeed * Time.deltaTime);
		}
		passaje = false;
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Player")) {
			if (portalOpen) {
				passaje = true;
				portalOpen = false;
				PlayerStatus playerStatus = other.gameObject.GetComponent<PlayerStatus> ();
				playerStatus.statusActionReg &= 252;
				playerStatus.statusActionReg |= 4;
				playerStatus.TraceAction (new PlayerDestination(other.transform.position,PlayerDestination.DestinationType.Portal));
				//playerStatus.TraceAction (player.transform.position);
				playerStatus.statusActionReg &= 248;
				Vector3 destination = new Vector3 (portalExit.position.x, other.transform.position.y, portalExit.position.z);
				//Vector3 destination = new Vector3 (portalExit.position.x, player.transform.position.y, portalExit.position.z);
				//Debug.Log("Portal Destination: "+destination);
				OnGoToPass ();
				//player.GetComponent<NavMeshAgent>().ResetPath();
				other.gameObject.GetComponent<PlayerMovement> ().setTeleportDestiny (destination);
				//player.GetComponent<PlayerMovement> ().setTeleportDestiny (destination);

				//playerNav.transform.position = destination;
				//player.transform.Translate(destination,Space.World);
				other.transform.rotation = portalExit.rotation;
				other.transform.Rotate(0,180,0);
				playerStatus.TraceAction (new PlayerDestination(destination,PlayerDestination.DestinationType.None));
				//player.transform.rotation = portalExit.rotation;

				//Debug.Log ("######Player Position: "+player.transform.position);



			}
		} /*else if (other.CompareTag ("Echo")) {
			Debug.Log ("transportando echo: " + other.tag);
			//other.gameObject.GetComponent<MeshRenderer>().enabled=false;
			//Destroy (other.gameObject);

		}*/
	}
}
