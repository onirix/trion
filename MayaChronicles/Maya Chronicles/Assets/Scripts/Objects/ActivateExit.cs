﻿using UnityEngine;
using System.Collections;

public class ActivateExit : MonoBehaviour
{
	public delegate void ExitActivated (bool activated);
	public static event ExitActivated OnExitActivated;
	public ParticleSystem emiter;
	//UserWin userWin;
	Behaviour halo;
	Vector3 yposition;
	Vector3 iposition;
	float smooth = 2f;
	bool characterOn = false;
	 
	void Start ()
	{
		GameObject exitPortal = GameObject.FindGameObjectWithTag ("Finish");
		halo = (Behaviour)exitPortal.GetComponent ("Halo");
		//userWin = exitPortal.GetComponent<UserWin> ();
		iposition = transform.position;
		yposition = new Vector3(transform.position.x,-8f,transform.position.z);
	}

	void FixedUpdate(){
		if (characterOn){
			if (Vector3.Distance(transform.position,yposition)>0.1f){
				transform.position = Vector3.Lerp(transform.position, yposition, smooth * Time.deltaTime);
			}else{
				characterOn = false;
			}
		}else{
			if(Vector3.Distance(transform.position,iposition)>0.1f){
				transform.position = Vector3.Lerp(transform.position,iposition,smooth * Time.deltaTime);
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		//Debug.Log("Player Exit Activate");
		if (other.CompareTag ("Player") || other.CompareTag ("Echo")) {
			characterOn = emiter.enableEmission = halo.enabled = true;
			if (OnExitActivated!=null)
				OnExitActivated(true);
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (other.CompareTag("Player")||other.CompareTag("Echo"))
		{
			characterOn = true;
		}
	}

	void OnTriggerExit (Collider other)
	{
		//Debug.Log("Player Exit Desactivated");
		if (other.CompareTag ("Player") || other.CompareTag ("Echo")) {
			emiter.enableEmission = halo.enabled = false;
			if (OnExitActivated!=null)
				OnExitActivated(false);
		}
	}
}
