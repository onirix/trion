﻿using UnityEngine;
using System.Collections;

public class FallDeath : MonoBehaviour {
	public delegate void PlayerFallDeath ();
	public static event PlayerFallDeath OnFallDeath;

	void OnTriggerEnter(Collider other){
		if (other.CompareTag("Player")){
			OnFallDeath();
		}
	}
}
