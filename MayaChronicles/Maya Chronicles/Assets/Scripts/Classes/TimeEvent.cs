﻿using UnityEngine;
using System.Collections;

public class TimeEvent {

	public float timeMoment = 0;
	public byte actionsReg = 0;
	public PlayerDestination playerTarget;
	public Quaternion rotation;
	
	public TimeEvent ()
	{
	}
	
	public TimeEvent (float timeMoment, byte actionsReg, PlayerDestination playerTarget/*, Quaternion rotation*/)
	{
		this.timeMoment = timeMoment;
		this.actionsReg = actionsReg;
		this.playerTarget = playerTarget;
		//this.rotation = rotation;
	}
}	