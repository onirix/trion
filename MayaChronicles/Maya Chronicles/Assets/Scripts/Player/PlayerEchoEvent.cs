using UnityEngine;
using System.Collections;

public class PlayerEchoEvent : MonoBehaviour
{
	public delegate void EvenDispached (int indexEcho);
	public static event EvenDispached EventReady;

	PlayerEchoHealth echoHealth;
	PlayerEchoMovement mov;
	TimeEvent timeEvent;
	public int indexEcho;
	Ray shootRay;                                   // A ray from the gun end forwards.
	RaycastHit shootHit;                            // A raycast hit to get information about what was hit.
	int shootableMask;
	public int damagePerShot = 20;                  // The damage inflicted by each bullet.
	public float timeBetweenBullets = 0.15f;        // The time between each shot.
	public float range = 100f;                      // The distance the gun can fire.
	ParticleSystem gunParticles;                    // Reference to the particle system.
	LineRenderer gunLine;                           // Reference to the line renderer.
	AudioSource gunAudio;                           // Reference to the audio source.
	Light gunLight;                                 // Reference to the light component.
	float effectsDisplayTime = 0.2f;
	float timer;
	bool onMyWay = false;
	
	void Awake ()
	{
		shootableMask = LayerMask.GetMask ("Shootable");
		echoHealth = GetComponent <PlayerEchoHealth> ();
		//nav = GetComponent <NavMeshAgent> ();
		mov = GetComponent <PlayerEchoMovement> ();
		/*gunParticles = GetComponentInChildren<ParticleSystem> ();
		gunLine = GetComponentInChildren <LineRenderer> ();
		gunAudio = GetComponentInChildren<AudioSource> ();
		gunLight = GetComponentInChildren<Light> ();*/
	}
	
	void Update ()
	{
		if (echoHealth.currentHealth <= 0){

		}
		timer += Time.deltaTime;
		if (timeEvent != null) {
			//if (playerHealth.currentHealth > 0 && timeEvent != null) {
			if (onMyWay) {
				if (Vector2.Distance (new Vector2 (transform.position.x, transform.position.z), new Vector2 (timeEvent.playerTarget.destination.x, timeEvent.playerTarget.destination.z)) < 1f) {
					EventReady (indexEcho);
					onMyWay = false;
				}
			} else {
				if (timer >= timeEvent.timeMoment) {
					if (mov.navAgent.enabled) {
						//nav.destination = timeEvent.position;
						mov.SetDestination (timeEvent.playerTarget);
						onMyWay = true;
					}
				}
			}
		} else {
			//nav.Stop ();
		}
		/*if (timer >= timeBetweenBullets * effectsDisplayTime) {
			DisableEffects ();
		}*/
	}

	void OnTriggerStay (Collider other)
	{
		if (other.CompareTag ("Enemy") && timer >= timeBetweenBullets) {
			//Vector3 direction = transform.position - other.transform.position;
			//Shoot (Vector3.Normalize(direction));
		}
	}

	public void DisableEffects ()
	{
		gunLine.enabled = false;
		gunLight.enabled = false;
	}

	void Shoot (Vector3 direction)
	{

		shootRay.origin = transform.position;
		shootRay.direction = direction;
		//shootRay.direction = transform.forward;

		if (Physics.Raycast (shootRay, out shootHit, range, shootableMask)) {
			timer = 0f;
			
			gunAudio.Play ();
			
			gunLight.enabled = true;
			
			gunParticles.Stop ();
			gunParticles.Play ();
			
			gunLine.enabled = true;
			gunLine.SetPosition (0, transform.position);
			EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
			
			if (enemyHealth != null) {
				enemyHealth.TakeDamage (damagePerShot, shootHit.point);
			}
			
			gunLine.SetPosition (1, shootHit.point);
		} else {
			gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
		}
	}

	public void setTargetEvent (TimeEvent timeEvent, int indexEcho)
	{
		this.timeEvent = timeEvent;
		this.indexEcho = indexEcho;
		//Debug.Log("New destination: "+timeEvent.playerTarget.destination);
	}
}
