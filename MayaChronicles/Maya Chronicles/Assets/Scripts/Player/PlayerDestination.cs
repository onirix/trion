﻿using UnityEngine;
using System.Collections;

public class PlayerDestination : MonoBehaviour {
	public enum DestinationType
	{
		None,
		Enemy,
		Jump,
		Portal,
		Block
	} 
	public Vector3 destination;
	public DestinationType target;
	public PlayerDestination(Vector3 destination, DestinationType target){
		this.destination = destination;
		this.target = target;
	}
}
