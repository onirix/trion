﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(NavMeshAgent))]
[RequireComponent (typeof(Animator))]
public class PlayerEchoMovement : MonoBehaviour
{

	float speed = 6f;
	float timer = 0;
	float timePause = 4f;
	float airTime = 2f;
	public Animator animator;
	public enum MoveState
	{
		Idle,
		Walking,
		Push,
		Pull,
		Jump
	}
	
	public bool teleportState = false;
	bool isStopTime = false;
	PlayerDestination playerTarget;
	public MoveState moveState;
	public NavMeshAgent navAgent;
	Rigidbody rigidbody;
	float animationSpeed;
	float jumpForce = 350;
	bool isOnAir = false;
	//public bool onJump = false;
	
	void Awake ()
	{
		animator = GetComponent<Animator> ();
		navAgent = GetComponent <NavMeshAgent> ();
		rigidbody = GetComponent <Rigidbody> ();
		animationSpeed = animator.speed;
		playerTarget = new PlayerDestination (transform.position, PlayerDestination.DestinationType.None);
		moveState = MoveState.Idle;
	}
	
	void Update ()
	{
		if (!UserWin.levelwin) {
			if (navAgent.enabled) {
				if (moveState == MoveState.Jump) {
				} else {
					if (navAgent.hasPath) {
						moveState = MoveState.Walking;
					} else {
						if (playerTarget.target != PlayerDestination.DestinationType.Jump)
							moveState = MoveState.Idle;
					}
					float angleForward = Vector3.Angle (transform.forward, playerTarget.destination);
					animator.SetInteger ("isMove", (int)moveState);
					if (playerTarget.target == PlayerDestination.DestinationType.Jump) {
						animator.SetFloat ("Speed", 7);
					} else {
						animator.SetFloat ("Speed", Mathf.Clamp (navAgent.remainingDistance, 5, 7));
					}
				}
			}
		} else {
			moveState = MoveState.Idle;
			animator.SetInteger ("isMove", (int)moveState);
			navAgent.enabled = false;
		}
	}
	
	void OnAnimatorMove ()
	{
		if (moveState == MoveState.Walking) {
			navAgent.velocity = animator.deltaPosition / Time.deltaTime;
			Quaternion lookRotation = Quaternion.LookRotation (navAgent.desiredVelocity);
			transform.rotation = Quaternion.RotateTowards (transform.rotation, lookRotation, navAgent.angularSpeed * Time.deltaTime);
		}
	}
	
	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Jump")) {
			//onJump = true;
			if (!isOnAir && moveState == MoveState.Walking && playerTarget.target == PlayerDestination.DestinationType.Jump && animator.GetFloat ("Speed") >= 1f) {
				Debug.Log (name + "Salte " + other.name);
				//animator.speed = 0;
				moveState = MoveState.Jump;
				navAgent.enabled = false;
				//transform.LookAt(destination);
				transform.rotation = other.transform.rotation;
				rigidbody.drag = 0;
				rigidbody.angularDrag = 0.05f;
				animator.SetInteger ("isMove", (int)moveState);
				//OnDisableJumps ();
				animator.SetFloat ("Speed", 0.5f);
				return;
			}
		}
		if (isOnAir) {
			Debug.Log ("Aterrice");
			rigidbody.velocity = Vector3.zero;
			rigidbody.angularVelocity = Vector3.zero; 
			rigidbody.drag = System.Single.PositiveInfinity;
			rigidbody.angularDrag = System.Single.PositiveInfinity;
			isOnAir = false;
			animator.speed = animationSpeed;
			navAgent.enabled = true;
			moveState = MoveState.Idle;
			animator.SetInteger ("isMove", (int)moveState);
			return;
		}
	}
	
	/*void OnTriggerExit (Collider other)
	{
		if (other.CompareTag ("Jump")) {
			onJump = false;
		}
	}*/
	
	public void JumpForce ()
	{
		rigidbody.AddForce ((transform.forward + transform.up) * jumpForce);
	}
	
	public void onAir ()
	{
		animator.speed = 0;
		isOnAir = true;
	}

	
	/*public void setStatus (MoveState state)
	{
		moveState = state;
		animator.SetInteger ("isMove", (int)state);
	}
	
	public void setTeleportDestiny (Vector3 destiny)
	{
		navAgent.ResetPath ();
		moveState = MoveState.Idle;
		animator.SetInteger ("isMove", (int)moveState);
		animator.SetFloat ("Speed", 0);
		navAgent.Warp (destiny);
	}*/
	
	public void SetDestination (PlayerDestination playerTarget)
	{
		Debug.Log ("Enviando destino");
		//if (moveState != MoveState.Jump) {
		Debug.Log ("Nuevo destino: " + playerTarget.destination);
		this.playerTarget = playerTarget;
		moveState = MoveState.Walking;
		navAgent.destination = playerTarget.destination;
		//}
	}
}
