﻿using UnityEngine;
using System.Collections;

public class PlayerStatus : MonoBehaviour
{
	public delegate void TriggerAction (byte actionsReg,PlayerDestination playerTarget);
	public static event TriggerAction OnAction;

	public delegate void DestinyChanged (PlayerDestination playerTarget);
	public static event DestinyChanged OnTargetChanged;

	public byte statusActionReg;
	PlayerMovement playerMovement;                              // Reference to the player's movement.
	public PlayerShooting playerShooting;

	void Awake ()
	{
		playerMovement = GetComponent <PlayerMovement> ();
		playerShooting = GetComponentInChildren <PlayerShooting> ();
		statusActionReg = 128;
	}

	void Start(){
		TraceAction(new PlayerDestination(transform.position, PlayerDestination.DestinationType.None));
	}

	public void Paralice ()
	{
		// Turn off the movement and shooting scripts.
		playerMovement.enabled = false;
		playerShooting.enabled = false;
	}

	public bool isOnAction ()
	{
		if (OnAction != null) {
			return true;
		}
		return false;
	}

	public void TraceAction (PlayerDestination destination)
	{
		OnAction (statusActionReg, destination);
	}

	public void ModifyDestiny(PlayerDestination.DestinationType target){
		OnTargetChanged (new PlayerDestination(transform.position,target));
	}
}
