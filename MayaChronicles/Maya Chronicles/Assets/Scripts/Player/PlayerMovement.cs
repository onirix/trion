﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
	public delegate void DisableJumps ();

	public static event DisableJumps OnDisableJumps;

	float speed = 6f;
	float timer = 0;
	float timePause = 4f;
	float airTime = 2f;
	public Animator animator;
	public enum MoveState
	{
		Idle,
		Walking,
		Push,
		Pull,
		Jump
	}

	public bool teleportState = false;
	bool isStopTime = false;
	PlayerDestination playerTarget;
	PlayerStatus playerStatus;
	public MoveState moveState;
	public NavMeshAgent navAgent;
	Rigidbody rigidbody;
	float animationSpeed;
	float jumpForce = 350;
	bool isOnAir = false;
	//bool ImOnTheFloor = true;
	public bool readyToJump = true;
	public bool onJump = false;

	void Awake ()
	{
		animator = GetComponent<Animator> ();
		navAgent = GetComponent <NavMeshAgent> ();
		rigidbody = GetComponent <Rigidbody> ();
		animationSpeed = animator.speed;
		playerTarget = new PlayerDestination (transform.position, PlayerDestination.DestinationType.None);
		moveState = MoveState.Idle;
		if (CompareTag ("Player"))
			playerStatus = GetComponent<PlayerStatus> ();
	}

	void Update ()
	{
		/*if (CompareTag("Echo"))
			Debug.Log (name+" Echo stat: "+moveState+" navAgent>enabled: "+navAgent.enabled+" speed: "+animator.GetFloat("Speed")+" isMOve: "+animator.GetInteger("isMove"));*/
		if (!UserWin.levelwin) {
			if (navAgent.enabled) {
				//Debug.Log("State: "+moveState);
				if (moveState == MoveState.Jump) {
					//timer+=Time.deltaTime;
				} else {
					//Debug.Log ("State: " + moveState);
					if (navAgent.hasPath) {
						/*float angle = Vector3.Angle (destination,transform.forward) * Mathf.Sign (Vector3.Cross (destination,transform.forward).y);
			Debug.Log ("Angle: "+angle);
			if (angle > 135 || angle < -135) {
				//navAgent.enabled = false;
				//moveState = MoveState.Back;
				moveState = MoveState.Turning;
			} else if (angle > 45) {
				//navAgent.enabled = false;
				//moveState = MoveState.Left;
				moveState = MoveState.Turning;
			} else if (angle < -45) {
				//navAgent.enabled = false;
				//moveState = MoveState.Right;
				moveState = MoveState.Turning;
			} else {*/
						//if (navAgent.isOnOffMeshLink) {
						//	moveState = MoveState.Jump;
						//} else {
						moveState = MoveState.Walking;
						//Debug.Log ("Walking: " + moveState);
						//}
						//navAgent.destination = destination;
						//}
					} else {
						/*if (moveState != MoveState.Idle && moveState != MoveState.Walking) {
						float angle = Vector3.Angle (transform.forward, destination) * Mathf.Sign (Vector3.Cross (transform.forward, destination).y);
						Debug.Log ("angle: "+angle+" moveState: "+moveState);
						if (angle < 45 && angle > -45) {
							//navAgent.enabled = true;
							//navAgent.destination = destination;
							moveState = MoveState.Walking;
							animator.SetInteger ("isMove", (int) moveState);
						}
					} else {*/
						if (playerTarget.target != PlayerDestination.DestinationType.Jump)
							moveState = MoveState.Idle;
						//animator.SetInteger ("isMove", (int) moveState);
						//}
					}
					float angleForward = Vector3.Angle (transform.forward, playerTarget.destination);
					//Debug.Log(name + ": angleForward: "+angleForward);
					animator.SetInteger ("isMove", (int)moveState);
					if (playerTarget.target == PlayerDestination.DestinationType.Jump) {
						//if (angleForward > 90)
						//	animator.SetFloat ("Speed", 1);
						//else
						animator.SetFloat ("Speed", 7);
					} else {
						//if (angleForward > 90)
						//	animator.SetFloat ("Speed", 1);
						//else
						animator.SetFloat ("Speed", Mathf.Clamp (navAgent.remainingDistance, 5, 7));
					}
				}
			}
		} else {
			moveState = MoveState.Idle;
			animator.SetInteger ("isMove", (int)moveState);
			navAgent.enabled = false;
		}
	}

	void OnAnimatorMove ()
	{
		if (moveState == MoveState.Walking) {
			navAgent.velocity = animator.deltaPosition / Time.deltaTime;
			Quaternion lookRotation = Quaternion.LookRotation (navAgent.desiredVelocity);
			transform.rotation = Quaternion.RotateTowards (transform.rotation, lookRotation, navAgent.angularSpeed * Time.deltaTime);
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.CompareTag ("Jump")) {
			onJump = true;
			if (!isOnAir && (CompareTag ("Echo") || readyToJump) && animator.GetFloat ("Speed") >= 1f) {
				if (CompareTag ("Player")) {
					Debug.Log ("Cambiando destino a jump");
					playerStatus.ModifyDestiny (PlayerDestination.DestinationType.Jump);
				}
				Debug.Log (name + "Salte " + other.name);
				//animator.speed = 0;
				moveState = MoveState.Jump;
				navAgent.enabled = false;
				//transform.LookAt(destination);
				transform.rotation = other.transform.rotation;
				rigidbody.drag = 0;
				rigidbody.angularDrag = 0.05f;
				animator.SetInteger ("isMove", (int)moveState);
				//OnDisableJumps ();
				//ImOnTheFloor = false;
				readyToJump = false;
				animator.SetFloat ("Speed", 0.5f);
				return;
			}
		}
		/*if (other.CompareTag("RotateFloor")){
			//Debug.Log ("Toque piso: "+other.name);
			other.GetComponent<RotateObject>().playerOn=true;
		}*/
		if (isOnAir) {
			Debug.Log ("Aterrice");
			rigidbody.velocity = Vector3.zero;
			rigidbody.angularVelocity = Vector3.zero; 
			rigidbody.drag = System.Single.PositiveInfinity;
			rigidbody.angularDrag = System.Single.PositiveInfinity;
			isOnAir = false;
			animator.speed = animationSpeed;
			navAgent.enabled = true;
			moveState = MoveState.Idle;
			animator.SetInteger ("isMove", (int)moveState);
			//navAgent.destination = transform.position+transform.forward;
			//Debug.Log ("destination: "+navAgent.destination+", "+transform.position);
			//timer=0;
			return;
		}
		/*if (animator.speed == 0) {

		}*/
	}

	void OnTriggerExit (Collider other)
	{
		if (other.CompareTag ("Jump")) {
			onJump = false;
		}
		/*if (other.CompareTag("RotateFloor")){
			//Debug.Log ("Ya no toco piso");
			other.GetComponent<RotateObject>().playerOn=false;
		}*/
	}

	void OnEnable ()
	{
		//RotateObject.OnDisableNav += DisableNav;
		//RotateObject.OnPlayerOnTheFloor += PlayerOnTheFloor;
	}
	
	void OnDisable ()
	{
		//RotateObject.OnDisableNav -= DisableNav;
		//RotateObject.OnPlayerOnTheFloor -= PlayerOnTheFloor;
	}

	/*void DisableNav (bool disable)
	{
		Debug.Log ("Disable: " + disable);
		//navAgent.enabled = disable;
	}*/

	/*void PlayerOnTheFloor ()
	{
		ImOnTheFloor = true;
	}*/

	public void JumpForce ()
	{
		rigidbody.AddForce ((transform.forward + transform.up) * jumpForce);
	}

	public void onAir ()
	{
		animator.speed = 0;
		isOnAir = true;
	}

	/*public void Landing()
	{
		animator.Play();
	}*/

	public void setStatus (MoveState state)
	{
		moveState = state;
		animator.SetInteger ("isMove", (int)state);
	}

	public void setTeleportDestiny (Vector3 destiny)
	{
		navAgent.ResetPath ();
		///navAgent.enabled = false;
		moveState = MoveState.Idle;
		animator.SetInteger ("isMove", (int)moveState);
		animator.SetFloat ("Speed", 0);
		navAgent.Warp (destiny);
		//transform.position = destiny;
		//navAgent.enabled = true;
	}

	public void SetDestination (PlayerDestination playerTarget)
	{
		if (moveState != MoveState.Jump) {
			this.playerTarget = playerTarget;
			/*float angle = Vector3.Angle (destination,transform.forward) * Mathf.Sign (Vector3.Cross (destination,transform.forward).y);
		if (angle > 135 || angle < -135) {
			//navAgent.enabled = false;
			//moveState = MoveState.Back;
			moveState = MoveState.Turning;
		} else if (angle > 45) {
			//navAgent.enabled = false;
			//moveState = MoveState.Left;
			moveState = MoveState.Turning;
		} else if (angle < -45) {
			//navAgent.enabled = false;
			//moveState = MoveState.Right;
			moveState = MoveState.Turning;
		} else {*/
			moveState = MoveState.Walking;
			//navAgent.destination = destination;
			//}
			navAgent.destination = playerTarget.destination;
			//animator.SetInteger ("isMove", (int) moveState);
			//Debug.Log ("angle: "+angle+" moveState: "+moveState);
		}
	}
}
