﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerEchoHealth : MonoBehaviour {
	public delegate void FindNewTimeLine (int indexEcho);
	public static event FindNewTimeLine OnFindNewTimeLine;
	
	public int startingHealth = 100;                            // The amount of health the player starts the game with.
	public int currentHealth;                                   // The current health the player has.
	//public Slider healthSlider;                                 // Reference to the UI's health bar.
	public Image damageImage;                                   // Reference to an image to flash on the screen on being hurt.
	public AudioClip deathClip;                                 // The audio clip to play when the player dies.
	public float flashSpeed = 5f;                               // The speed the damageImage will fade at.
	public Color flashColour = new Color(1f, 0f, 0f, 0.1f);     // The colour the damageImage is set to, to flash.
	public byte statusActionReg;
	
	Animator anim;                                              // Reference to the Animator component.
	AudioSource playerAudio;                                    // Reference to the AudioSource component.
	PlayerStatus playerStatus;                                  // Reference to the PlayerStatus script.
	bool isDead;                                                // Whether the player is dead.
	bool damaged;                                               // True when the player gets damaged.
	
	
	void Awake ()
	{
		currentHealth = startingHealth;
		anim = GetComponent <Animator> ();
		playerAudio = GetComponent <AudioSource> ();
		playerStatus = GetComponent <PlayerStatus> ();
		damageImage = GameObject.Find("DamageImage").GetComponent<Image>();
	}
	
	
	void Update ()
	{
		if(damaged)
		{
			damageImage.color = flashColour;
		}
		else
		{
			damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
		}
		damaged = false;
	}
	
	
	public void TakeDamage (int amount)
	{
		damaged = true;
		currentHealth -= amount;
		//healthSlider.value = currentHealth;
		playerAudio.Play ();
		
		if(currentHealth <= 0 && !isDead)
		{
			Death ();
			OnFindNewTimeLine(GetComponent<PlayerEchoEvent>().indexEcho);
		}
	}
	
	
	void Death ()
	{
		isDead = true;
		playerStatus.playerShooting.DisableEffects ();
		anim.SetTrigger ("Die");
		playerAudio.clip = deathClip;
		playerAudio.Play ();
		
		playerStatus.Paralice();
	} 
}
